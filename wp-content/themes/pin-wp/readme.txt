Version: 1.7.1 - September 18, 2015
-------------------------------------------
Added: Social sharing icons for the category / tags / search, etc. pages.
Fixed: Space between the content and the footer.

Files Changed:
-------------
- /style.css
- /readme.txt
- /index.php
- /css/colors/default.css





Version: 1.7 - September 18, 2015
-------------------------------------------
New: WordPress 4.3+ Compatibility.
New: Social sharing icons ( article page ).
New: Social sharing icons ( home page ).
New: Full Date added ( Day of Month / Month / Year ) for better structure Data ( article page ).
Updated: To latest font awesome version 4.4.
Improved: Slider loading and height image issue.
Improved: Google Snippets
Fixed: The Called Constructor Method For WP_Widget Is Deprecated.
Fixed: Warnings from Theme Check (Pin WP passed the tests).
Removed: Social Share button ( Plugin removed by the Author from WordPress repository ).
Removed: Google fonts plugin ( It has not been updated for more than a year, there are better plugins ).

Files Changed:
-------------
- /style.css
- /readme.txt
- /single.php
- /functions.php
- /functions/scripts.php
- /functions/custom/class-tgm-plugin-activation.php
- /css/responsive.css
- /admin/front-end/options.php
removed: /css/font-awesome-4.3.0
added: /css/font-awesome-4.4.0





Version: 1.6 - April 28, 2015
-------------------------------------------
- New: Option to display only once, the ADs from the homepage.
- Updated: TGM Plugin Activation class for security vulnerability.
- Updated: Documentation.

Files Changed:
-------------
- /style.css
- /readme.txt
- /admin/functions/functions.options.php
- /index.php
- /template-home.php
- /functions/custom/class-tgm-plugin-activation.php   (@version   2.4.2)






Version: 1.5 - April 17, 2015
-------------------------------------------
- New: Anonymous / Guest front end posting without login / plugin.
- Updated: Documentation.
- Updated: .XML Demo content.

Files Changed:
-------------
- /style.css
- /readme.txt
- /css/colors/default.css 
- /css/responsive.css
- /header.php
- /admin/functions/functions.options.php
- /functions/scripts.php
- /functions.php







Version: 1.4 - March 18, 2015
-------------------------------------------
- Improved: Minor CSS and HTML improvements, calendar, links, lists, etc.
- New: New Widget: Posts by Tags (write the tag, or add more tags).
- Updated: To latest font awesome version 4.3.

Files Changed:
-------------
- /style.css
- /css/colors/default.css 
- /readme.txt 
- /functions.php
- /functions/scripts.php
added: /functions/widgets/widget-posts-tags.php
removed: /css/font-awesome-4.2.0
added: /css/font-awesome-4.3.0






Version: 1.3 - February 19, 2015
-------------------------------------------
- New: Google Webmaster added Missing requires for SERP/SEO "entry-title", "updated", "author", etc.
- New: Homepage ads, you can replace 300x250 with 300x600.
- Improved: Caption for Gallery Images.

Files Changed:
-------------
- /style.css 
- /readme.txt 
- /css/responsive.css
- /template-home.php
- /index.php
- /single.php






Version: 1.2 - February 11, 2015
-------------------------------------------
- New: Display Caption for Gallery Images.
- Improved: Theme improved for smaller devices.

Files Changed:
-------------
- /style.css 
- /readme.txt 
- /single.php
- /fancybox/jquery.fancybox-1.3.4.css
- /css/responsive.css






Version: 1.1 - February 08, 2015
-------------------------------------------
- New: 2 new Ad Spots for the homepage.
- Improved: Increased the width for small devices.
- Fixed: the issues with the ads being cut off at the bottom.

Files Changed:
-------------
- /style.css 
- /readme.txt 
- /css/responsive.css
- /template-home.php
- /index.php
- /admin/functions/functions.options.php






Version: 1.0
-------------------------------------------
Theme Name: Pin
Theme URI: http://themeforest.net/user/An-Themes/portfolio
Author: An-Themes
Follow me: http://themeforest.net/user/An-Themes/follow for more Magazine Themes!