<?php

$min = PRICE_MIN;
$max = PRICE_MAX;

//    $start = microtime(true);
//dd(microtime(true) - $start);

$whereCategory = is_category() ? " AND $wpdb->term_taxonomy.term_id IN ( " . get_category(get_query_var('cat'))->term_id . " ) " : "";

$key = 'filterbrands' . md5($whereCategory);
$cached = get_transient($key);
if (!$cached) {

    $querystr = "
                SELECT wpostmeta.meta_value as brand, count(*) as count
                FROM $wpdb->posts wposts
                    LEFT JOIN $wpdb->postmeta wpostmeta ON  wposts.ID = wpostmeta.post_id
                    LEFT JOIN $wpdb->term_relationships ON (wposts.ID = $wpdb->term_relationships.object_id)
                    LEFT JOIN $wpdb->term_taxonomy      ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
                WHERE
                    $wpdb->term_taxonomy.taxonomy = 'category'
                    AND wpostmeta.meta_key = '_brand'
                      $whereCategory
                    AND post_status = 'publish'
                GROUP BY wpostmeta.meta_value
                ORDER BY wpostmeta.meta_value ASC
                ";

    $categoriesAll = get_categories([
        'type' => 'post',
        'child_of' => 0,
        'parent' => '',
        'orderby' => 'count',
        'order' => 'DESC',
        'hide_empty' => 1,
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'number' => '',
        'taxonomy' => 'category',
        'pad_counts' => false
    ]);

    $brands = [];
    foreach ($wpdb->get_results($querystr) as $brand) {

        foreach ($categoriesAll as $category) {
            if ($category->name == $brand->brand) {
                $brands[] = [
                    'id' => $category->cat_ID,
                    'name' => $category->name,
                    'count' => $brand->count
                ];
                break;
            }
        }
    }


    usort($brands, function ($a, $b) {
        return ((int)$a['count'] < (int)$b['count']) ? 1 : -1;
    });

    $brands = array_slice($brands, 0, 50);

    usort($brands, function ($a, $b) {
        return $a['name'] < $b['name'] ? -1 : 1;
    });

    set_transient($key, $brands, 60 * 60 * 12);
} else {
    $brands = $cached;
}

$selectedBrands = get_input('brands', []);


?>

<div class="sticky-filter">

    <div class="search-form cf">
        <form action="" class="js-filter-form" data-min="<?php echo $min ?>" data-max="<?php echo $max ?>">

        <span class="filter-range">
            Price range: <span class="filter-amount" style=""></span>
        </span>
            <div class="search-form-price">
                <div class="search-form-price-labels">
                    <input type="hidden" name="price_high" value="<?php echo get_input('price_high') ?>">
                    <input type="hidden" name="price_low" value="<?php echo get_input('price_low') ?>">
                </div>
                <div id="slider-range-price"></div>
            </div>

            <div class="search-form-select search-form-order" data-auto-close="true">
                <div class="search-form-placeholder order-placeholder">Select Order</div>
                <div class="search-form-dropdown">
                    <div class="search-form-inner">
                        <label class="js-set-order" data-order="">
                            <input type="radio" name="order"
                                   value="" <?php echo get_input('order') == '' ? 'checked="checked"' : '' ?>>
                            Latest First
                        </label>
                        <label class="js-set-order" data-order="ASC">
                            <input type="radio" name="order"
                                   value="ASC" <?php echo get_input('order') == 'ASC' ? 'checked="checked"' : '' ?>>
                            Low Price First
                        </label>
                        <label class="js-set-order" data-order="DESC">
                            <input type="radio" name="order"
                                   value="DESC" <?php echo get_input('order') == 'DESC' ? 'checked="checked"' : '' ?>>
                            High Price First
                        </label>
                    </div>
                </div>
            </div>
            <div class="search-form-select">
                <div class="search-form-placeholder brand-placeholder">Select Brands</div>
                <div class="search-form-dropdown">
                    <div class="search-form-inner">
                        <?php foreach ($brands as $brand): ?>
                            <label>
                                <input type="checkbox" class="search-form-brands-checkbox"
                                       name="brands[]"
                                       title="<?php echo $brand['name'] ?>"
                                       value="<?php echo $brand['id'] ?>" <?php echo(in_array($brand['id'], $selectedBrands) ? 'checked="checked"' : '') ?>/>

                                <?php printf('%s (%d)', $brand['name'], $brand['count']) ?>
                            </label>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <button class="filter-apply">Apply</button>
        </form>

        <script type="text/javascript">
        </script>
    </div>
</div>
